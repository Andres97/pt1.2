package ex2;

import java.io.IOException;
import java.util.Scanner;

/**
 * Archivo que contiene el menu del ejercicio
 * @version 3.0
 * @data 23/10/18
 */
public class Ex2 {
    /**
     * Array que contiene las opciones del menu
     */
    private String[] menu = {
        "Dar la vuelta",
        "Encontrar la base mas repetida",
        "Encontrar la base menos repetida",
        "Recuentos de bases",
        "Salir"
    };
    /**
     * Main que crea un objecto ex2 y crea una funcion para llamar al menu
     * @param args
     * @throws IOException excepcion de lectura de fichero
     */
    public static void main(String[] args) throws IOException {
        Ex2 ex = new Ex2();
        ex.run();
    }
    /**
     * funcion que se encarga de la seleccion de las diferentes opciones del menu
     * @throws IOException 
     */
    private void run() throws IOException{
        //opcion por defecto para salir
        int optionSelect = 5;
        //mientras opcion sea diferente a 5
        do{
            optionSelect = showMenu();
            //opciones del menu
            switch(optionSelect){
                case 1: System.out.println("1 ");
                        break;
                case 2: System.out.println("2 ");
                        break;
                case 3: Opciones.MenosRepetido();
                        break;
                case 4: Opciones.Recuento();
                        break;
                case 5: System.out.println("Has salido del menu");
                        break;
                default:
                    System.out.println("Opcion incorrecta");
                        break;
            }
        }while(optionSelect!=5);
    }
    /**
     * funcion que se encarga de mostrar el menu y
     * pregunta al usuario la opcion a elegir
     * @return opcion elegida por el usuario
     */
    private int showMenu() {
        int option = -1;
        //recorre el array e imprime las diferentes opciones
        System.out.println("Menu");
        for (int i = 0; i < menu.length; i++) {
            System.out.format("%d %s \n", i+1, menu[i]);
        }

        Scanner sc = new Scanner(System.in);
        
        System.out.print("Choose an option: ");
        option = sc.nextInt();
        
        return option;
    }
    
}

/*
 * Archivo que contiene las funciones 3 y 4 sobre la lectura del archivo
 */
package ex2;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Opciones {

    /**
     * funcion que se encarga de mostar cual es la base menos repetida
     *
     * @throws FileNotFoundException excepcion de error de lectura de archivo
     */
    public static void MenosRepetido() throws FileNotFoundException {
        //Variable que guarda en un string el contenido del archivo
        String cadena = MostrarContenido.leerArchivo();

        //separa cada letra y la guarda en un array
        String[] ADN = cadena.split("");

        //contador el cual ira comparando si es menor
        int menorRepeticion = 50;
        
        //variable donde guardara cual es la base menos repetida
        String baseMenosRepetida = "";
        
        //reccoremos el array
        for (String bases : ADN) {
            //inicializamos contador a 0
            int cont = 0;
            
            //base la cual ira comparando
            String baseActual = bases;
            
            //solo comprobara la menos repetida si es ATCG
            if (bases.matches("[ATCG]")) {
                //recorremos otra vez el array para compararlo con la actual
                for (String basesN : ADN) {
                    //comprueba si son iguales
                    if (basesN.equals(baseActual)) {
                        cont++;
                    }
                }
                //si el contador es menor al contador por defecto lo guarda
                if (menorRepeticion > cont) {
                    menorRepeticion = cont;
                    baseMenosRepetida = baseActual;
                }
            }
        }

        System.out.println(baseMenosRepetida + ": Se repite " + menorRepeticion);
    }

    /**
     * Funcion que se encarga de mostrar el recuento de bases del archivo
     *
     * @throws IOException excepcion de error de lectura de archivo
     */
    public static void Recuento() throws IOException {

        //Variable que guarda en un string el contenido del archivo
        String cadena = MostrarContenido.leerArchivo();

        //separa cada letra y la guarda en un array
        String[] ADN = cadena.split("");

        //contadores de bases
        int contA = 0;
        int contT = 0;
        int contC = 0;
        int contG = 0;

        //contrador por si hay alguna letra aparte de ATCG
        int contOtro = 0;

        //recorre el array uno por uno
        for (String bases : ADN) {
            //dependiendo de la letre que encuentre suma 1 al contador
            switch (bases) {
                case "A":
                    contA++;
                    break;
                case "T":
                    contT++;
                    break;
                case "C":
                    contC++;
                    break;
                case "G":
                    contG++;
                    break;
                default:
                    contOtro++;
                    break;
            }
        }

        //Imprime el recuento total
        System.out.println("Recuento total de bases");
        System.out.println("Adenina = " + contA);
        System.out.println("Timina = " + contT);
        System.out.println("Citosina = " + contC);
        System.out.println("Guanina = " + contG);
        System.out.println("Otro = " + contOtro);

    }
}

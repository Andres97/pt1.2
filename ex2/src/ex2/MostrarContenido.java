package ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Archivo que se encarga de guardar en una variable el contenido del archivo
 *
 * @author andres
 * @version 3.0
 * @data 23/10/18
 */
public class MostrarContenido {

    /**
     * funcion que lee el archivo y lo guarda en un string
     *
     * @return cadena el contenido del archivo
     * @throws FileNotFoundException excepcion de error de lectura de archivo
     */
    public static String leerArchivo() throws FileNotFoundException {
        //crea un objecto que contiene la ruta o nombre del archivo
        File archivo = new File("adn.txt");

        //cadena que contendra el archivo
        String cadena = null;

        //objecto para leer el archivo
        FileReader f = new FileReader(archivo);
        Scanner sc = new Scanner(f);
        //mientras haya una linea
        while (sc.hasNextLine()) {
            //cadena guarda el contenido
            cadena = sc.nextLine();
        }
        //devuelve cadena
        return cadena;
    }

}
